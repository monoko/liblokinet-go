package liblokinet_test

import (
	"log"
	"testing"

	"github.com/monok-o/liblokinet-go"
)

func TestStart(t *testing.T) {
	liblokinet.Start()

	address := liblokinet.Addr()
	log.Println(address)
}
